// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/CharacterMovementComponent.h"
#include "CGCharacterMovementComponent.generated.h"

/**
 * XXX Currently no different from UCharacterMovementComponent!
 * The Movement component for Crystalline Characters.
 */
UCLASS()
class CRYSTALLINE_API UCGCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()
};
