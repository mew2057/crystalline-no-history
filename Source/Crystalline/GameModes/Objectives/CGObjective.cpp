// Fill out your copyright notice in the Description page of Project Settings.

#include "Crystalline.h"
#include "CGObjective.h"


// Sets default values
ACGObjective::ACGObjective(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;
	SetRemoteRoleForBackwardsCompat(ROLE_SimulatedProxy);
	bReplicates = true;
	bReplicateMovement = true;

}

// Called when the game starts or when spawned
void ACGObjective::BeginPlay()
{
	Super::BeginPlay();
	bool bInitSucceeded = true;

	// If this is on the server, tell the Game Mode of our existence.
	UWorld* World = GetWorld();
	if (World && World->GetAuthGameMode())
	{
		ACGBaseGameMode* GameMode = Cast<ACGBaseGameMode>(World->GetAuthGameMode());
		if (GameMode)
		{
			bInitSucceeded = GameMode->InitObjective(this);
		}
	}

	// If the initialization failed, destroy this object.
	if (!bInitSucceeded)
	{
		Destroy();
	}
}


