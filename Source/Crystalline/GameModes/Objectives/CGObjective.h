// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "CGObjective.generated.h"

UCLASS()
class CRYSTALLINE_API ACGObjective : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACGObjective(const FObjectInitializer& ObjectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	bool IsAHill() const { return bKotHZone; }

protected:
	// TODO replace with class check.
	/** This objective may be classified as a king of the hill zone. */
	UPROPERTY(EditAnywhere, Category = "Options")
	uint8 bKotHZone : 1;



};
