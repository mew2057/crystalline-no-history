// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "CGGameSettings.generated.h"

/**
 * 
 */
UCLASS()
class CRYSTALLINE_API UCGGameSettings : public UGameUserSettings
{
	GENERATED_BODY()

public:
	UCGGameSettings(const FObjectInitializer& ObjectInitializer);

	
	
};
