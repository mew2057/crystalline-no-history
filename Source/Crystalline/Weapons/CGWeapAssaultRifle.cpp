// Fill out your copyright notice in the Description page of Project Settings.

#include "Crystalline.h"
#include "CGWeapAssaultRifle.h"

ACGWeapAssaultRifle::ACGWeapAssaultRifle(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	WeaponConfig.bUsesProjectile = false;
	WeaponConfig.AmmoType = ECGAmmoType::T_ZERO;
}

