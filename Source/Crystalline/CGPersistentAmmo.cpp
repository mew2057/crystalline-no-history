// Fill out your copyright notice in the Description page of Project Settings.

#include "Crystalline.h"
#include "CGPersistentAmmo.h"

// Sets default values
ACGPersistentAmmo::ACGPersistentAmmo(const FObjectInitializer& ObjectInitializer)
{ 	
	Ammo = 10;
	AmmoType = ECGAmmoType::T_ONE;
	bIsActive = true;
	TimeToRespawn = 5;
	
	SetRemoteRoleForBackwardsCompat(ROLE_SimulatedProxy);
	bReplicates = true;

	AmmoMesh = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("AmmoMesh"));
	AmmoMesh->bReceivesDecals = false;
	AmmoMesh->CastShadow = true;
	AmmoMesh->bOnlyOwnerSee = false;
	AmmoMesh->bOwnerNoSee = false;
	AmmoMesh->SetCollisionObjectType(ECC_WorldDynamic);
	AmmoMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	AmmoMesh->SetCollisionResponseToAllChannels(ECR_Ignore);
	AmmoMesh->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	AmmoMesh->SetCollisionObjectType(COLLISION_AMMO_DROP);
	AmmoMesh->SetHiddenInGame(false);
	AmmoMesh->SetIsReplicated(true);
	AmmoMesh->SetSimulatePhysics(false);
	AmmoMesh->RelativeLocation = FVector(0.f, 0.f, 0.f);
	RootComponent = AmmoMesh;
}

void ACGPersistentAmmo::ReceiveActorBeginOverlap(class AActor* Other)
{
	if (!bIsActive)
		return;

	if (Role == ROLE_Authority)
	{
		ACGCharacter* Player = Cast<ACGCharacter>(Other);
		// If the player is given any ammo, kill this object.
		if (Player && Player->GiveAmmo(AmmoType, Ammo))
		{
			bIsActive = false;
			AmmoMesh->SetHiddenInGame(true);
			GetWorldTimerManager().SetTimer(TimerHandle_Respawn, this, &ACGPersistentAmmo::OnRespawn, TimeToRespawn, false);
		}
	}
}

void ACGPersistentAmmo::OnRespawn()
{ 
	GetWorldTimerManager().ClearTimer(TimerHandle_Respawn);
	bIsActive = true;
	AmmoMesh->SetHiddenInGame(false);
}

void ACGPersistentAmmo::OnRep_Active()
{
	if (bIsActive)
	{
		AmmoMesh->SetHiddenInGame(false);
	}
	else
	{
		AmmoMesh->SetHiddenInGame(true);
	}
}

void ACGPersistentAmmo::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ACGPersistentAmmo, bIsActive);
}
