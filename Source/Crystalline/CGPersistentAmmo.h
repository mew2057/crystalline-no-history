// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Misc/CGTypes.h"
#include "CGPersistentAmmo.generated.h"

UCLASS()
class CRYSTALLINE_API ACGPersistentAmmo : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACGPersistentAmmo(const FObjectInitializer& ObjectInitializer);

	/** Invoked once the respawn timer is completed.*/
	void OnRespawn();

	/** Hides or shows the crystal based upon it's activity state. */
	UFUNCTION()
	void OnRep_Active();
	
	/**
	* Getter for the ammo type.
	*
	* @return The Type of ammo this pickup represents.
	*/
	FORCEINLINE ECGAmmoType GetAmmoType() const { return AmmoType; };

	/**
	* Gives the ammo to the player if collided.
	*/
	virtual void ReceiveActorBeginOverlap(class AActor* Other) override;

protected:
	/** The amount of ammo in the pickup.*/
	UPROPERTY(EditAnywhere, Category = Config)
	int32 Ammo;

	/** The type of ammo that this weapon supplies.*/
	ECGAmmoType AmmoType;

	/** Tracks whether or not the ammo can be picked up. */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_Active)
	uint32 bIsActive : 1;

	/** Defines the visual component for the ammo pickup.*/
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	UMeshComponent* AmmoMesh;

	/** Time to respawn after pickup, if set to zero the ammo is never exhausted. */
	UPROPERTY(EditAnywhere, Category = Config)
	float TimeToRespawn;

	/** Timer Handle for the Respawn timer.*/
	FTimerHandle TimerHandle_Respawn;
};
